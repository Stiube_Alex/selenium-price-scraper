import selenium.webdriver as webdriver
from selenium.webdriver.firefox.service import Service
import json
import time
import telepot

# Switch this path to your webdriver
firefox_driver = "C:\Program Files\Browser Drivers\geckodriver.exe"
firefox_service = Service(firefox_driver)
browser = webdriver.Firefox(service=firefox_service)


# You will need a file to save the token key and another one to save the telegram user or bot id
key = open("token.key")
token = key.read()
key.close()

key = open("telegramId.key")
receiver_id = key.read()
key.close()

telegramBot = telepot.Bot(token)


# You will need a json file to save the product url and desired price
def loadData():
    file = open("products.json")
    data = json.load(file)
    file.close()
    return data


def checkPrice(url, myPrice):
    time.sleep(1)
    browser.get(url)

    try:
        title = browser.find_element(
            "xpath", '//h1[contains(@class, "page-title")]'
        ).get_attribute("innerText")

        price = browser.find_element(
            "xpath",
            '(//div[contains(@class, "pricing-block")]//p[contains(@class, "product-new-price")])[2]',
        )

        theirPrice = int(
            price.get_attribute("innerText").split(",")[0].replace(".", "")
        )
    except:
        print("\n\n Element not visible !!! \n\n")
        return False

    # Format the message sent as you wish in the 'message' variable
    if theirPrice <= myPrice:
        message = (
            "\n" + title + "\n\n" + "Price:  " + str(theirPrice) + " RON" + "\n\n" + url
        )
        telegramBot.sendMessage(receiver_id, message)
        print(message)


for product_data in loadData()["products"]:
    checkPrice(product_data["url"], product_data["price"])

browser.quit()
