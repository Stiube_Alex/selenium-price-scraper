# eCommerce Price Scraper

An example tool that visits multiple web pages on a web store. The tool examines the price for individual products and send a message to a telegram bot if the price is below your budget.

## API Reference

We are using a telegram bot where the mesages are send.

Both parameters are required for app to work ! They will be stored in 2 individual `.key` files.

| Parameter     | Type     | Description                                                   |
| :------------ | :------- | :------------------------------------------------------------ |
| `token`       | `string` | Telegram token / Your API key                                 |
| `receiver_id` | `string` | Telegram user id / Id of the user to whom the message is sent |

## Installation

1. Install python from [here](https://www.python.org/downloads/).

1. Install selenium with pip:

   ```python
   pip install selenium
   ```

1. Download geckodriver from [here](https://github.com/mozilla/geckodriver/releases).

1. Extract the content from the driver to a location from where you will need to copy the path into code.

1. Modify the content of `firefox_driver` with you driver path location.

**NOTE**: You also need to have the firefox installed !

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/Stiube_Alex/selenium-price-scraper.git
```

Go to the project directory

```bash
  cd selenium-price-scraper
```

Run the script

```python
  python script.py
```

## Tech Stack

**Code:** Python 3.10.0 / Selenium: 4.6.0

**Browser:** Firefox 106.0.5 (64-bit)

**Driver:** geckodriver 0.32.0 win (64-bit)

**OS:** Windows 11 21H2 (64-bit)
